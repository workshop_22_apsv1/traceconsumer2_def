package es.upm.dit.apsv.traceconsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import java.util.function.Consumer;
import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;

import es.upm.dit.apsv.traceconsumer.model.*;
import es.upm.dit.apsv.traceconsumer.Repository.*;
import es.upm.dit.apsv.traceconsumer.controller.TraceController;
@SpringBootApplication
public class TraceconsumerApplication {

    public static final Logger log = LoggerFactory.getLogger(TraceconsumerApplication.class);

    private TraceRepository torderRepository;

    public static void main(String[] args) {
        SpringApplication.run(TraceconsumerApplication.class, args);
    }

    @Bean("consumer")

        public Consumer<Trace> checkTrace() {

                return t -> {

                        t.setTraceId(t.getTruck() + t.getLastSeen());

                        torderRepository.save(t);

             };

        }

    
}
